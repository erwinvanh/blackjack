"""
dealer.py 2019-02-19
"""
from BlackJack.hand import Hand


class Dealer(Hand):
    """
    Class containing the dealer-information
    """

    def __init__(self):
        super().__init__()
        self.bank = 1000
        self.outstanding_bet = 0

    def reset_hand(self):
        """
        Reset the dealer's hand
        :return:
        """
        Hand.reset(self)
        self.outstanding_bet = 0

    def bank_wins(self, chips):
        """
        Add the chips the player has lost to the bank
        :param chips:
        :return:
        """
        self.bank += chips

    def bank_loses(self, chips):
        """
        Pay the player from the bank
        :param chips:
        :return:
        """
        if self.bank >= chips:
            self.bank -= chips
            return True
        return False
