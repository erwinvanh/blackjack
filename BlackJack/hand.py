"""
hand.py 2019-02-19
"""


class Hand:
    """
    Class containing the hand of cards (either player or bank)
    """
    def __init__(self):
        self.cards = []  # start with an empty list as we did in the Deck class
        self.value = 0  # start with zero value
        self.aces = 0  # add an attribute to keep track of aces

    def add_card(self, card):
        """
        Add a card to the hand
        :param card:
        :return:
        """
        self.cards.append(card)
        self.value += card.get_value()
        if card.rank == "Ace":
            self.aces += 1
        # If hand exceeds 21 try to adjust with aces
        if self.value > 21:
            self.adjust_for_ace()

    def adjust_for_ace(self):
        """
        Change ace-value from 11 to 1 when needed
        :return:
        """
        if self.aces > 0:
            self.aces -= 1
            self.value -= 10

    def reset(self):
        """
        Reset the hand
        :return:
        """
        self.cards = []
        self.value = 0
        self.aces = 0

    def __str__(self):
        string = ''
        for card in self.cards:
            string = string + str(card)
