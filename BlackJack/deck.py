"""
Deck : 2019-02-19
"""
import random
from BlackJack.card import Card

SUITS = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
RANKS = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine',
         'Ten', 'Jack', 'Queen', 'King', 'Ace')


class Deck:
    """
    Class holding a deck of cards (52)
    """
    def __init__(self):
        self.deck = []  # start with an empty list
        for suit in SUITS:
            for rank in RANKS:
                card = Card(suit, rank)
                self.deck.append(card)
        self.deck = self.deck * 4
        # Random value which decides if new deck must be created
        # Deck contains 4 *52 cards, pick value between 25 and 100
        self.lower_limit = random.randint(25, 100)

    def __str__(self):
        string = 'Deck contains : \n'
        for card in self.deck:
            string = string + str(card) + '\n'

        return string

    def shuffle(self):
        """
        Shuffle the deck
        :return:
        """
        random.shuffle(self.deck)

    def deal(self):
        """
        Hand 1 card to player or dealer
        :return:
        """
        return self.deck.pop()
