"""
player.py 2019-02-19
"""
from BlackJack.hand import Hand


class Player(Hand):
    """
    Class containing the player-information
    """

    def __init__(self, name, chips):
        super().__init__()
        self.name = name
        self.chips = chips
        self.hand = Hand()
        self.busted = False
        self.playing = True

    def reset_hand(self):
        """
        Reset the player's hand and related information
        :return:
        """
        Hand.reset(self)
        if self.chips.total > 0:
            self.playing = True
        self.busted = False
