"""
chips.py 2019-02-19
"""


class Chips:
    """
    Class holding the chips for a player
    """

    def __init__(self, total=100):
        self.total = total
        self.bet = 0

    def win_bet(self, dealer):
        self.total += self.bet
        dealer.bank_loses(self.bet)

    def lose_bet(self, dealer):
        self.total -= self.bet
        dealer.bank_wins(self.bet)
