"""
Card : 2019-02-19
"""

VALUES = {'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5, 'Six': 6, 'Seven': 7, 'Eight': 8,
          'Nine': 9, 'Ten': 10, 'Jack': 10, 'Queen': 10, 'King': 10, 'Ace': 11}


class Card:
    """
    Class containing one single card
    """

    def __init__(self, suit, rank):
        """
        Create one card based upon suit and rank
        """
        self.suit = suit
        self.rank = rank

    def __str__(self):
        """
        :return: Printable version of a card
        """
        return self.rank + " of " + self.suit

    def get_value(self):
        """
        Get the value of this card
        """
        return VALUES[self.rank]
