"""
Main class for the blackjack-game
"""

from BlackJack.deck import Deck
from BlackJack.chips import Chips
from BlackJack.player import Player
from BlackJack.dealer import Dealer


def take_bet(player):
    """
    Take a player's bet
    """
    while True:
        try:
            amount = int(input(f'{player.name}, please enter bet-amount '))
        except ValueError:
            print("Enter a valid amount")
        else:
            # Can the player afford it ?
            if amount <= player.chips.total:
                if dealer.outstanding_bet + amount <= dealer.bank:
                    player.chips.bet = amount
                    dealer.outstanding_bet += amount
                    break
                else:
                    print('Bank can\'t cover this bet. Maximum is ' + str(dealer.bank - dealer.outstanding_bet))
            else:
                print(f'Maximum amount is {player.chips.total}')


def hit(hand, deck):
    """
    Hand a card to player or dealer
    :param hand: hand-object in player or dealer-object
    :param deck:
    :return:
    """

    hand.add_card(deck.deal())


def hit_or_stand(deck, player):
    """
    Ask player to hit or stand, auto-stand when player
    reaches 21 points
    :param deck:
    :param player:
    :return:
    """
    while True:
        action = input(player.name + " hit or stand ? ")
        if action.upper().startswith("H"):
            hit(player, deck)
            if player.value == 21:
                # Stand if value = 21
                player.playing = False
            break
        elif action.upper().startswith("S"):
            print('Player ' + player.name + ' stands')
            player.playing = False
            break


def show_some():
    """
    Show 1 card of the dealer and all cards for the player(s)
    :return: None
    """
    print("\nDealer's Hand:")
    print(" <card hidden>")
    print('', dealer.cards[1])
    for player in players:
        print(f"\n{player.name}\'s Hand:", *player.cards, sep='\n ')
        print(f'Player\'s hand-total = {player.value}')


def show_all():
    """
    Show all cards for both dealer and player(s)
    :return: None
    """
    print('\nDealer\'s Hand:', *dealer.cards, sep='\n ')
    print(f'Dealer\'s Hand = {dealer.value}')
    for player in players:
        print(f'\n{player.name}\'s Hand :', *player.cards, sep='\n ')
        print(f'Player\'s Hand-total = {player.value}')


def player_busts(player):
    """
    Player has busted, perform the necessary actions
    :param player:
    :return: none
    """
    player.chips.lose_bet(dealer)
    player.busted = True
    player.playing = False
    print('Too bad, you busted ' + player.name)


def player_wins(player):
    """
    Player has won, update chip-count for dealer and player
    :param player:
    :return:
    """
    player.chips.win_bet(dealer)
    print('Yes, you have won ' + player.name + ' !')


def dealer_busts(player):
    """
    Dealer has busted
    :param player:
    :return:
    """
    player.chips.win_bet(dealer)
    print('Dealer has busted, you have won ' + player.name + ' !')


def dealer_wins(player):
    """
    Dealer wins, update chip-total for
    player and dealer
    :param player:
    :param dealer:
    :return:
    """
    player.chips.lose_bet(dealer)
    print('Too bad, you have lost ' + player.name + ' !')


def push(player):
    """
    It's a draw, cancel outstanding bet
    :param player:
    :return:
    """
    print('It\'s a draw, ' + player.name + ' bet cancelled')
    player.chips.bet = 0


def setup_players():
    """
    Setup the players-array
    :return:
    """
    while True:
        try:
            number_of_players = int(input("Enter number of players :"))
        except ValueError:
            print('Enter a valid number ')
        else:
            if number_of_players > 4:
                print("Maximum number of players = 4")
            else:
                break
    ii = 0
    while ii < number_of_players:
        player_name = input('Enter name for player ' + str((ii+1)))
        players.append(Player(player_name, Chips(200)))
        ii += 1


if __name__ == '__main__':
    # Create empty array to store the players
    players = []
    setup_players()
    # Print an opening statement
    print("Welcome to the blackjack-game. This game can be played with 1 to 4 players. "
          "If player has more points than the bank player has won, if player exceeds 21 points "
          "player has lost.")
    new_deck = Deck()
    dealer = Dealer()

    while True:
        num_busted = 0
        # Shuffle the deck
        new_deck.shuffle()

        # Deal 2 cards to each player
        for current_player in players:
            hit(current_player, new_deck)
            hit(current_player, new_deck)
        # Deal 2 cards to the dealer
        hit(dealer, new_deck)
        hit(dealer, new_deck)

        # Prompt the Player for their bet
        for current_player in players:
            take_bet(current_player)

        # Show cards (but keep one dealer card hidden)
        show_some()

        for current_player in players:
            while current_player.playing:
                # Prompt for Player to Hit or Stand
                hit_or_stand(new_deck, current_player)
                # Show cards (but keep one dealer card hidden)
                show_some()
                # If player's hand exceeds 21, run player_busts() and break out of loop
                if current_player.value > 21:
                    player_busts(current_player)
                    num_busted += 1
                    break

        # If at least one player hasn't busted, play Dealer's hand until Dealer reaches 17
        if num_busted < len(players):
            print('It\'s up to the dealer now')
            while dealer.value < 17:
                hit(dealer, new_deck)

        # Show all cards
        show_all()
        print("********************************************************************")

        # Run different winning scenarios
        # Iterate backwards to make it possible to delete elements from the list
        for i in range(len(players) - 1, -1, -1):
            current_player = players[i]
            if not current_player.busted:
                if dealer.value > 21:
                    dealer_busts(current_player)
                elif current_player.value > dealer.value:
                    player_wins(current_player)
                elif dealer.value > current_player.value:
                    dealer_wins(current_player)
                else:
                    push(current_player)
            # Inform Player of their chips total
            print(f'{current_player.name} you have got ' + str(current_player.chips.total)
                  + ' chips.')
            if current_player.chips.total == 0:
                print("You can no longer play, you are bankrupt !")
                del players[i]
            else:
                # Check if player wants to keep playing
                new_game = input('Would you like to play again ? (Y)es or (N)o')
                if new_game.lower()[0] == "n":
                    del players[i]

        if dealer.bank == 0:
            print('Bank is busted, game stops')
            break

        # Continue game while there are players left
        # Player can either stop or go bankrupt
        if players:
            # Check if we need a new deck of cards
            if len(new_deck.deck) < new_deck.lower_limit:
                print("Creating new deck of cards")
                new_deck = Deck()
            for current_player in players:
                current_player.reset_hand()
            dealer.reset_hand()
            continue
        else:
            break
